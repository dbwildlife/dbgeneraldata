--/*********************************************/
--/        Création d'un serveur étranger       /
--/       depuis les données WFS de l'INPN      /
--/*********************************************/
-- Pour la méthode, se référer au fichier inpn_znir.sql
-- Dans un terminal, lancez la commande suivante:
--
--# ogrinfo "WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0"
--
-- ERROR 1: Server is read-only WFS; no WFS-T feature advertized
-- Had to open data source read-only.
-- INFO: Open of `WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0'
-- using driver `WFS' successful.
-- 1: sa:CoursEau
-- 2: sa:PlanEau
-- 3: sa:CoursEau1
-- 4: sa:CoursEau2
-- 5: sa:CoursEau3
-- 6: sa:CoursEau4
-- 7: sa:CoursEau5
-- 8: sa:CoursEau6
-- 9: sa:CoursEau7
-- 10: sa:RegionHydro
-- 11: sa:SecteurHydro
-- 12: sa:SousSecteurHydro
-- 13: sa:ZoneHydro
-- 14: sa:LaisseEaux_BassesEaux
-- 15: sa:LaisseEaux_HautesEaux
-- 16: sa:Limite300m
-- 17: sa:Limite1Nq
-- 18: sa:TronconHydrographique
-- Les couches qui nous intéressent sont la première et la seconde
-- 
-- On interroge le serveur sur le contenu de la couche 1: sa:CoursEau
--# ogrinfo -al -so "WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0?TYPENAME=sa:CoursEau"
--
-- CREATROR 1: Server is read-only WFS; no WFS-T feature advertized
-- Had to open data source read-only.
-- INFO: Open of `WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0?TYPENAME=sa:CoursEau'
-- using driver `WFS' successful.
-- Layer name: sa:CoursEau
-- Geometry: Unknown (any)
-- ^[[A^[[A^[[AFeature Count: 126654
-- Extent: (41.370566, -5.096482) - (51.119942, 9.557704)
-- Layer SRS WKT:
-- GEOGCS["WGS 84",
-- DATUM["WGS_1984",
-- SPHEROID["WGS 84",6378137,298.257223563,
-- AUTHORITY["EPSG","7030"]],
-- AUTHORITY["EPSG","6326"]],
-- PRIMEM["Greenwich",0,
-- AUTHORITY["EPSG","8901"]],
-- UNIT["degree",0.0174532925199433,
-- AUTHORITY["EPSG","9122"]],
-- AUTHORITY["EPSG","4326"]]
-- Geometry Column = msGeometry
-- gml_id: String (0.0)
-- CdEntiteHydrographique: String (0.0)
-- NomEntiteHydrographique: String (0.0)
-- Classe: String (0.0)

CREATE
	SCHEMA IF NOT EXISTS fdw AUTHORIZATION lpo07;

DROP
	FOREIGN TABLE
		IF EXISTS fdw.sandre_cours_eau;

CREATE
	FOREIGN TABLE
		fdw.sandre_cours_eau(
			fid INT,
			geom geometry,
			gml_id VARCHAR,
			cdentitehydrographique VARCHAR,
			classe VARCHAR(1)
		) SERVER sandre_wfs OPTIONS(
			layer 'sa:CoursEau'
		);

CREATE
	TABLE
		occsol.courseau AS SELECT
			*
		FROM
			fdw.sandre_cours_eau;
