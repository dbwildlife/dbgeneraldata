-- Compilation et installation de l'extension ogr_fdw
-- depuis ce lien : https://github.com/pramsey/pgsql-ogr-fdw
-- Création d'un schéma dédié aux tables distantes

CREATE
	SCHEMA IF NOT EXISTS fdw;


DROP
	FOREIGN TABLE
		IF EXISTS fdw_soes_clc2012;

-- Création de la table distante fdw_soes_clc2012 contenant les données de la base de données CorineLandCover 2012 pour la france métropolitaine.

CREATE
	FOREIGN TABLE
		fdw.fdw_soes_clc12(
			fid BIGINT,
			geom Geometry(
				MultiPolygon,
				2154
			),
			gml_id VARCHAR,
			id VARCHAR,
			code_12 VARCHAR,
			area_ha REAL
		) SERVER soes_clc_wfs OPTIONS(
			layer 'clc:CLC12'
		);

-- Vérification du nombre de points de la couche.

DROP
	TABLE
		IF EXISTS landuse.soes_clc12;

CREATE
	TABLE
		landuse.soes_clc12 AS(
			SELECT
				*
			FROM
				fdw.fdw_soes_clc12,
				
		);

ALTER TABLE
	landuse.soes_clc12 ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_soes_clc12 ON
	landuse.soes_clc12
		USING gist(geom);
