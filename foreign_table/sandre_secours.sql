--/*********************************************/
--/        Création d'un serveur étranger       /
--/       depuis les données WFS de l'INPN      /
--/*********************************************/
-- Pour la méthode, se référer au fichier inpn_znir.sql
-- Dans un terminal, lancez la commande suivante:
--# ogrinfo "WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0"
-- ERROR 1: Server is read-only WFS; no WFS-T feature advertized
-- Had to open data source read-only.
-- INFO: Open of `WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0'
-- using driver `WFS' successful.
-- 1: sa:CoursEau
-- 2: sa:PlanEau
-- 3: sa:CoursEau1
-- 4: sa:CoursEau2
-- 5: sa:CoursEau3
-- 6: sa:CoursEau4
-- 7: sa:CoursEau5
-- 8: sa:CoursEau6
-- 9: sa:CoursEau7
-- 10: sa:RegionHydro
-- 11: sa:SecteurHydro
-- 12: sa:SousSecteurHydro
-- 13: sa:ZoneHydro
-- 14: sa:LaisseEaux_BassesEaux
-- 15: sa:LaisseEaux_HautesEaux
-- 16: sa:Limite300m
-- 17: sa:Limite1Nq
-- 18: sa:TronconHydrographique
create
	foreign table
		fdw.sandre_cours_eau(
			fid int,
			geom geometry,
			gml_id varchar,
			cdentitehydrographique varchar,
			classe varchar(1)
		) SERVER sandre_wfs options(
			layer 'sa:CoursEau'
		);

create
	table
		occsol.courseau as select
			*
		from
			fdw.sandre_cours_eau;
