﻿ --/**********************************/
--/ Création d'un serveur étranger   /
--/ depuis les données texte TAXREF  /
--/**********************************/
-- Source http://si.cenlr.org/postgresql_fdw_taxref
-- Important, ce serveur nécessite d'avoir préalablement téléchargé les données PostgreSQL. cf. fichier foreign_table/taxref_table.sql 
-- Elles sont accessibles depuis cette adresse https://inpn.mnhn.fr/telechargement/referentielEspece/referentielTaxo
-- La version de TAXREF ici utilisée est la version 10.0
-- Le fichier TAXREFv10.0.txt doit être préalablement placé dans le dossier /tmp
-- Depuis le terminal : cp ./TAXREFv10.0.txt /tmp/
-- Création du schéma dédié aux données distantes si pas présent
create
	schema if not exists fdw;

-- Suppression de l'ancienne table distante taxref si présente
drop
	foreign table
		fdw.taxref;

-- Création de la table distante
create
	foreign table
		fdw.taxref(
			regne varchar,
			phylum varchar,
			classe varchar,
			ordre varchar,
			famille varchar,
			group1_inpn varchar,
			group2_inpn varchar,
			cd_nom integer,
			cd_taxsup integer,
			cd_sup integer,
			cd_ref integer,
			rang varchar,
			lb_nom varchar,
			lb_auteur varchar,
			nom_complet varchar,
			nom_complet_html varchar,
			nom_valide varchar,
			nom_vern varchar,
			nom_vern_eng varchar,
			habitat varchar,
			fr varchar,
			gf varchar,
			mar varchar,
			gua varchar,
			sm varchar,
			sb varchar,
			spm varchar,
			may varchar,
			epa varchar,
			reu varchar,
			sa varchar,
			ta varchar,
			taaf varchar,
			pf varchar,
			nc varchar,
			wf varchar,
			cli varchar,
			url varchar
		) SERVER txtfile_srv options(
			format 'csv',
			header 'true',
			filename '/tmp/TAXREFv10.0.txt',
			delimiter E'\t',
			null ''
		);

-- Création du schéma dédié aux référentiels si pas présent
create
	schema if not exists repos;
	

-- Suppression de la table distante si présente
drop
	table
		if exists repos.taxref;

-- Création de la nouvelle table locale taxref
create
	table
		repos.taxref as select
			*
		from
			fdw.taxref;

-- Création des index sur les champs courrament utilisés
create
	index taxref_cd_nom_idx on
	repos.taxref(cd_nom);

create
	index taxref_cd_ref_idx on
	repos.taxref(cd_ref);

create
	index taxref_rang_idx on
	repos.taxref(rang);

create
	index taxref_group2_inpn_idx on
	repos.taxref(group2_inpn);

create
	index taxref_fr_idx on
	repos.taxref(fr);
