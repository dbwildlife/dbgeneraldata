﻿ --/*********************************************/
--/	Création d'un serveur étranger		/
--/	depuis les données WFS de la DREAL RA	/
--/*********************************************/
-- Pour lister les tables du serveur depuis un terminal
--# ogrinfo "WFS:http://ws.carmen.developpement-durable.gouv.fr/WFS/30/NATURE_PAYSAGE_BIODIVERSITE_RA?"  
-- ERROR 1: Server is read-only WFS; no WFS-T feature advertized
-- Had to open data source read-only.
-- INFO: Open of `WFS:http://ws.carmen.developpement-durable.gouv.fr/WFS/30/NATURE_PAYSAGE_BIODIVERSITE_RA?'
--       using driver `WFS' successful.
-- 1: Departements
-- 2: Regions_voisines
-- 3: Plans_d_eau_principaux
-- 4: Zones_baties_principales
-- 5: Cours_d_eau_principaux
-- 6: Autoroutes
-- 7: Villes_principales
-- 8: Unites_paysageres
-- 9: Ouvrages_d_art_lineaires
-- 10: Ouvrages_d_art_ponctuels
-- 11: Jardins
-- 12: Znieff_de_type_2
-- 13: Znieff_de_type_1
-- 14: ZICO
-- 15: Tourbieres__sites_
-- 16: Tourbieres__bassins_
-- 17: SCAP_GRILLE_FINALE
-- 18: Habitats_elementaire_Natura_2000
-- 19: Zone_de_protection_1930
-- 20: Operation_Grand_Site
-- 21: Secteur_Sauvergarde
-- 22: Directive_saleve
-- 23: affichage_site_paysage
-- 24: Site_Inscrit
-- 25: Site_Classe
-- 26: Natura2000___ZPS
-- 27: Natura2000___SIC
-- 28: Zonage_RAMSAR
-- 29: Parc_National
-- 30: Reserve_Integrale_de_Parc_Naturel
-- 31: Parc_Naturel_Regional
-- 32: Reserve_Naturelle_Regionale
-- 33: Reserve_Naturelle_Nationale
-- 34: Arrete_Prefectorail_de_Protection_de_Biotope
-- Pour connaitre les champs de la couche, depuis un terminal
--# ogrinfo -al -so "WFS:http://ws.carmen.developpement-durable.gouv.fr/WFS/30/NATURE_PAYSAGE_BIODIVERSITE_RA?TYPENAME=Natura2000___SIC"
-- ERROR 1: Server is read-only WFS; no WFS-T feature advertized
-- Had to open data source read-only.
-- INFO: Open of `WFS:http://ws.carmen.developpement-durable.gouv.fr/WFS/30/NATURE_PAYSAGE_BIODIVERSITE_RA?TYPENAME=Natura2000___SIC'
--       using driver `WFS' successful.
-- 
-- Layer name: Natura2000___SIC
-- Geometry: Unknown (any)
-- Feature Count: 133
-- Extent: (3.566491, 44.038142) - (7.185041, 46.514028)
-- Layer SRS WKT:
-- GEOGCS["WGS 84",
--     DATUM["WGS_1984",
--         SPHEROID["WGS 84",6378137,298.257223563,
--             AUTHORITY["EPSG","7030"]],
--         AUTHORITY["EPSG","6326"]],
--     PRIMEM["Greenwich",0,
--         AUTHORITY["EPSG","8901"]],
--     UNIT["degree",0.0174532925199433,
--         AUTHORITY["EPSG","9122"]],
--     AUTHORITY["EPSG","4326"]]
-- Geometry Column = msGeometry
-- gml_id: String (0.0)
-- CD_SIG: String (0.0)
-- SPN: String (0.0)
-- NOM: String (0.0)
-- DEPARTEMENTS: String (0.0)
-- ZONE_BIOGEOG: String (0.0)
-- STATUT: String (0.0)
-- TRANSMISSION: String (0.0)
-- DATE_SIC: String (0.0)
-- DATE_ZSC: String (0.0)
-- SURF_DECLA_HA: String (0.0)
-- SURF_SIG_HA: String (0.0)
-- date_maj_sig: String (0.0)
-- DOCOB: String (0.0)
-- referentiel: String (0.0)
--Création d'un schema dédié aux données des Foreign Data Wrappers
create
	schema if not exists fdw authorization lpo07;

--Création de la table distante des parcs naturels marins
drop
	foreign table
		if exists fdw.dreal_ra_n2000_sic;

create
	foreign table
		fdw.dreal_ra_n2000_sic(
			fid integer,
			geom GEOMETRY,
			gml_id text,
			cd_sig text,
			spn text,
			nom text,
			departement text,
			zone_biogeo text,
			statut text,
			transmission text,
			date_sic text,
			date_zsc text,
			surf_decla_ha text,
			surf_sig_ha text,
			docob text,
			referentiel text
		) SERVER dreal_ra_biodiv_wfs options(
			layer 'Natura2000___SIC'
		);

-- Création d'un schéma destiné à recevoir l'ensemble des couches de "zones naturelles d'intérêt reconnu"
create
	schema if not exists znir authorization lpo07;

-- Création de la table locale des parcs naturels marins
drop
	table
		znir.dreal_ra_n2000_sic;

create
	table
		znir.dreal_ra_n2000_sic as(
			select
				fid,
				st_transform(
					st_setsrid(
						geom,
						4326
					),
					2154
				) geom,
				gml_id,
				cd_sig,
				spn,
				nom,
				departement,
				zone_biogeo,
				statut,
				transmission,
				date_sic,
				date_zsc,
				surf_decla_ha,
				surf_sig_ha,
				docob,
				referentiel
			from
				fdw.dreal_ra_n2000_sic
		);

alter table
	znir.dreal_ra_n2000_sic add primary key(fid);

create
	index gidx_dreal_ra_n2000_sic on
	znir.dreal_ra_n2000_sic
		using gist(geom);

-- Actualisation des géométries de la base de données
select
	Populate_Geometry_Columns();
