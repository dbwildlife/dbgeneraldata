﻿ -- Compilation et installation de l'extension ogr_fdw
-- depuis ce lien : https://github.com/pramsey/pgsql-ogr-fdw
-- Création d'un schéma dédié aux tables distantes
create
	schema if not exists fdw;

drop
	foreign table
		if exists fdw_brgm_bdcavites;

-- Création de la table distante fdw_brgm_bdcavites contenant les données de la base de données BDCavités.
create
	foreign table
		fdw.fdw_brgm_bdcavites(
			msGeometry geometry(
				Point,
				4326
			),
			gid text,
			num_insee text,
			identifiant text,
			nom_cavite text,
			date_validite text,
			type_cavite text,
			type_cavite_appauvri text,
			reperage_geographique text
		) SERVER brgm_risque options(
			layer 'CAVITE_LOCALISEE'
		);

-- Vérification du nombre de points de la couche (pour le moment, incomplet avec seulement 1000 tuples).
select
	count(*)
from
	fdw.fdw_brgm_bdcavites;
