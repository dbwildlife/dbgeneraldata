/****************************************
*	Création d'un serveur étranger		*
*	depuis les données WFS de l'INPN	*
*****************************************/
-- Pour lister les tables du serveur depuis un terminal
--# ogrinfo  "WFS:http://ws.carmencarto.fr/WFS/119/fxx_inpn?"
-- Résultat de la commande
--$INFO: Open of `WFS:http://ws.carmencarto.fr/WFS/119/fxx_inpn?' using driver `WFS' successful.
--$1: Sites_d_importance_communautaire_JOUE__ZSC_SIC_
--$2: Zones_de_protection_speciale
--$3: Sites_d_importance_communautaire
--$4: Znieff2_mer
--$5: Znieff1_mer
--$6: Znieff2
--$7: Znieff1
--$8: Bien_du_patrimoine_mondial_de_l_UNESCO
--$9: Reserves_de_la_biosphere
--$10: Reserves_naturelles_nationales
--$11: Sites_Ramsar
--$12: Terrains_des_Conservatoires_des_espaces_naturels
--$13: Terrains_du_Conservatoire_du_Littoral
--$14: Parc_naturel_marin
--$15: Parcs_naturels_regionaux
--$16: Reserves_nationales_de_chasse_et_faune_sauvage
--$17: Parcs_nationaux
--$18: Reserves_Integrales_de_Parcs_Nationaux
--$19: Reserves_naturelles_Corse
--$20: Reserves_naturelles_regionales
--$21: Arretes_de_protection_de_biotope
--$22: Reserves_biologiques
-- Pour connaitre les champs de la couche, depuis un terminal
--# ogrinfo -al -so "WFS:http://ws.carmencarto.fr/WFS/119/fxx_inpn?TYPENAME=Parc_naturel_marin"
-- Résultat de la commande
--$ INFO: Open of `WFS:http://ws.carmencarto.fr/WFS/119/fxx_inpn?TYPENAME=Parc_naturel_marin'
--$       using driver `WFS' successful.
--$ 
--$ Layer name: Parc_naturel_marin
--$ Geometry: Unknown (any)
--$ Feature Count: 5
--$ Extent: (75945.741100, 6148375.899900) - (774166.642400, 7081173.501000)
--$ Layer SRS WKT:
--$ PROJCS["RGF93 / Lambert-93",
--$     GEOGCS["RGF93",
--$         DATUM["Reseau_Geodesique_Francais_1993",
--$             SPHEROID["GRS 1980",6378137,298.257222101,
--$                 AUTHORITY["EPSG","7019"]],
--$             TOWGS84[0,0,0,0,0,0,0],
--$             AUTHORITY["EPSG","6171"]],
--$         PRIMEM["Greenwich",0,
--$             AUTHORITY["EPSG","8901"]],
--$         UNIT["degree",0.0174532925199433,
--$             AUTHORITY["EPSG","9122"]],
--$ 		AUTHORITY["EPSG","4171"]],
--$     PROJECTION["Lambert_Conformal_Conic_2SP"],
--$     PARAMETER["standard_parallel_1",49],
--$     PARAMETER["standard_parallel_2",44],
--$     PARAMETER["latitude_of_origin",46.5],
--$     PARAMETER["central_meridian",3],
--$     PARAMETER["false_easting",700000],
--$     PARAMETER["false_northing",6600000],
--$     UNIT["metre",1,
--$         AUTHORITY["EPSG","9001"]],
--$     AXIS["X",EAST],
--$     AXIS["Y",NORTH],
--$     AUTHORITY["EPSG","2154"]]
--$ Geometry Column = msGeometry < col1
--$ gml_id: String (0.0) < col2
--$ ID_MNHN: String (0.0) < col3
--$ NOM_SITE: String (0.0) < col4
--$ URL: String (0.0) < col5
--Création d'un schema dédié aux données des Foreign Data Wrappers
CREATE
	SCHEMA IF NOT EXISTS fdw AUTHORIZATION lpo07;

--Création de la table distante des parcs naturels marins
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_parc_naturel_marin;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_parc_naturel_marin(
			fid BIGINT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id VARCHAR,
			id_local VARCHAR,
			id_mnhn VARCHAR,
			nom_site VARCHAR,
			date_crea VARCHAR,
			modif_adm VARCHAR,
			modif_geo VARCHAR,
			url_fiche VARCHAR,
			surf_off VARCHAR,
			acte_deb VARCHAR,
			acte_fin VARCHAR,
			gest_site VARCHAR,
			operateur VARCHAR,
			precision_ VARCHAR,
			src_geom VARCHAR,
			src_annee VARCHAR,
			marin VARCHAR,
			p1_nature VARCHAR,
			p2_culture VARCHAR,
			p3_paysage VARCHAR,
			p4_geologi VARCHAR,
			p5_speleo VARCHAR,
			p6_archeo VARCHAR,
			p7_paleob VARCHAR,
			p8_anthrop VARCHAR,
			p9_science VARCHAR,
			p10_public VARCHAR,
			p11_dd VARCHAR,
			p12_autre VARCHAR,
			url VARCHAR
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Parc_naturel_marin'
		);

-- Création d'un schéma destiné à recevoir l'ensemble des couches de "zones naturelles d'intérêt reconnu"
CREATE
	SCHEMA IF NOT EXISTS naturalarea AUTHORIZATION lpo07;

-- Création de la table locale des parcs naturels marins
DROP
	TABLE
		IF EXISTS naturalarea.inpn_parc_naturel_marin;

CREATE
	TABLE
		naturalarea.inpn_parc_naturel_marin AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_parc_naturel_marin
		);

ALTER TABLE
	naturalarea.inpn_parc_naturel_marin ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_parc_naturel_marin ON
	naturalarea.inpn_parc_naturel_marin
		USING gist(geom);

-- Création de la table distante des parcs nationaux
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_parc_national;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_parc_national(
			fid BIGINT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id VARCHAR,
			id_mnhn VARCHAR,
			url VARCHAR,
			nom VARCHAR,
			ZONE VARCHAR
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Parcs_nationaux'
		);

-- Création de la table locale des parcs nationaux
DROP
	TABLE
		IF EXISTS naturalarea.inpn_parc_national;

CREATE
	TABLE
		naturalarea.inpn_parc_national AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_parc_national
		);

ALTER TABLE
	naturalarea.inpn_parc_national ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_parc_national ON
	naturalarea.inpn_parc_national
		USING gist(geom);

-- Création de la table distante des sites natura 2000 (SIC)
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_n2000_sic;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_n2000_sic(
			fid INT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id TEXT,
			sitecode TEXT,
			sitename TEXT,
			url TEXT
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Sites_d_importance_communautaire'
		);

--Création de la table locale des sites Natura 2000 (SIC)
DROP
	TABLE
		IF EXISTS naturalarea.inpn_n2000_sic;

CREATE
	TABLE
		naturalarea.inpn_n2000_sic AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_n2000_sic
		);

ALTER TABLE
	naturalarea.inpn_n2000_sic ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_n2000_sic ON
	naturalarea.inpn_n2000_sic
		USING gist(geom);

-- Création de la table distante des sites natura 2000 (ZPS)
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_n2000_zps;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_n2000_zps(
			fid BIGINT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id VARCHAR,
			sitecode VARCHAR,
			sitename VARCHAR,
			url VARCHAR
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Zones_de_protection_speciale'
		);

--Création de la table locale des sites Natura 2000 (ZPS)
DROP
	TABLE IF EXISTS naturalarea.inpn_n2000_zps;
		
CREATE
	TABLE
		naturalarea.inpn_n2000_zps AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_n2000_zps
		);

ALTER TABLE
	naturalarea.inpn_n2000_zps ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_n2000_zps ON
	naturalarea.inpn_n2000_zps
		USING gist(geom);

--Création de la table distante des ZNIEFF terrestres de type 1
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_znieff1;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_znieff1(
			fid BIGINT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id VARCHAR,
			id_mnhn VARCHAR,
			nom VARCHAR,
			url VARCHAR
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Znieff1'
		);

--Création de la table locale des ZNIEFF terrestres de type 1
DROP
	TABLE IF EXISTS 
		naturalarea.inpn_znieff1;
CREATE
	TABLE
		naturalarea.inpn_znieff1 AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_znieff1
		);

ALTER TABLE
	naturalarea.inpn_znieff1 ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_znieff1 ON
	naturalarea.inpn_znieff1
		USING gist(geom);

--Création de la table distante des ZNIEFF terrestres de type 2
DROP
	FOREIGN TABLE
		IF EXISTS fdw.fdw_inpn_znieff2;

CREATE
	FOREIGN TABLE
		fdw.fdw_inpn_znieff2(
			fid BIGINT,
			geom Geometry(
				Multipolygon,
				2154
			),
			gml_id VARCHAR,
			id_mnhn VARCHAR,
			nom VARCHAR,
			url VARCHAR
		) SERVER inpn_infogeo_wfs OPTIONS(
			layer 'Znieff2'
		);

--Création de la table locale des ZNIEFF terrestres de type 1
DROP
	TABLE IF EXISTS naturalarea.inpn_znieff2;
		
CREATE
	TABLE
		naturalarea.inpn_znieff2 AS(
			SELECT
				*
			FROM
				fdw.fdw_inpn_znieff2
		);

ALTER TABLE
	naturalarea.inpn_znieff2 ADD PRIMARY KEY(fid);

CREATE
	INDEX gidx_inpn_znieff2 ON
	naturalarea.inpn_znieff2
		USING gist(geom);
SELECT st_astext(geom) FROM naturalarea.inpn_znieff2 LIMIT 2;
-- Actualisation des géométries de la base de données
SELECT
	Populate_Geometry_Columns();
