CREATE SCHEMA IF NOT EXISTS fdw;

/************************
 * Grilles de l'INPN    *
 ************************/


/* Connection au serveur WFS */
CREATE SERVER inpn_grille
FOREIGN DATA WRAPPER ogr_fdw
OPTIONS (
  datasource 'WFS:http://ws.carmencarto.fr/WFS/119/fxx_grille?',
FORMAT 'WFS' );

/*
 * Maillage de 10kmx10km en Lambert 93
 */


/* Suppression de la table distante si existante */
DROP FOREIGN TABLE IF EXISTS fdw.l93_10x10;

/* Création de la table distante */
CREATE FOREIGN TABLE fdw.l93_10x10 (
  fid    BIGINT,
  geom   GEOMETRY(Geometry, 4326),
  gml_id VARCHAR
) SERVER inpn_grille
OPTIONS (layer 'L93_10X10'
);

/* Suppression de la table locale si existante */
DROP TABLE IF EXISTS repos.l93_10x10km;

/* Création de la table locale depuis la table distante */

CREATE TABLE repos.l93_10x10km AS
  SELECT *
  FROM fdw.l93_10x10;

ALTER TABLE repos.l93_10x10km
  ADD PRIMARY KEY (fid);

CREATE UNIQUE INDEX CONCURRENTLY l93_10x10km_fidx ON repos.l93_10x10km (fid);
ALTER TABLE repos.l93_10x10km DROP CONSTRAINT IF EXISTS l93_10x10km_pkey,
    ADD CONSTRAINT l93_10x10km_pkey PRIMARY KEY USING INDEX l93_10x10km_fidx;

/* Création de l'index spatial */

CREATE INDEX l93_10x10km_gidx
  ON repos.l93_10x10km USING GIST (geom);


SELECT populate_geometry_columns();

/*
 * Maillage de 10kmx10km en LA
 */


/* Suppression de la table distante si existante */
DROP FOREIGN TABLE IF EXISTS fdw.la_10x10;

/* Création de la table distante */
CREATE FOREIGN TABLE fdw.la_10x10 (
  fid       BIGINT,
  geom      GEOMETRY(Geometry, 4326),
  gml_id    VARCHAR,
  cd_sig    VARCHAR,
  code_10km VARCHAR
) SERVER inpn_grille
OPTIONS (layer 'LA_10x10'
);

/* Suppression de la table locale si existante */
DROP TABLE IF EXISTS repos.la_10x10km;

/* Création de la table locale depuis la table distante */

CREATE TABLE repos.la_10x10km AS
  SELECT *
  FROM fdw.la_10x10;

/* Création de la clé primaire */

CREATE UNIQUE INDEX CONCURRENTLY la_10x10km_fidx ON repos.la_10x10km (fid);
ALTER TABLE repos.la_10x10km DROP CONSTRAINT IF EXISTS la_10x10km_pkey,
    ADD CONSTRAINT la_10x10km_pkey PRIMARY KEY USING INDEX la_10x10km_fidx;



/* Création de l'index spatial */

CREATE INDEX la_10x10km_gidx
  ON repos.la_10x10km USING GIST (geom);

/*
 * Maillage de 5kmx5km en Lambert 93
 */


/* Suppression de la table distante si existante */
DROP FOREIGN TABLE IF EXISTS fdw.l93_5x5;

/* Création de la table distante */
CREATE FOREIGN TABLE fdw.l93_5x5 (
  fid     BIGINT,
  geom    GEOMETRY(Geometry, 4326),
  gml_id  VARCHAR,
  cd_sig  VARCHAR,
  code5km VARCHAR
) SERVER inpn_grille
OPTIONS (layer 'L93_5x5'
);


/* Suppression de la table locale si existante */
DROP TABLE IF EXISTS repos.l93_5x5km;

/* Création de la table locale depuis la table distante */

CREATE TABLE repos.l93_5x5km AS
  SELECT *
  FROM fdw.l93_5x5;

/* Création de l'index spatial */

CREATE INDEX l93_5x5km_gidx
  ON repos.l93_5x5km USING GIST (geom);


/*
 * Maillage de 1kmx1km en Lambert 93
 */


/* Suppression de la table distante si existante */
DROP FOREIGN TABLE IF EXISTS fdw.l93_1x1;

/* Création de la table distante */
CREATE FOREIGN TABLE fdw.l93_1x1 (
  fid       BIGINT,
  geom      GEOMETRY(Geometry, 4326),
  gml_id    VARCHAR,
  cd_sig    VARCHAR,
  code_10km VARCHAR
) SERVER inpn_grille
OPTIONS (layer 'L93_1X1'
);


/* Suppression de la table locale si existante */
DROP TABLE IF EXISTS repos.l93_1x1km;

/* Création de la table locale depuis la table distante */

CREATE TABLE repos.l93_1x1km AS
  SELECT *
  FROM fdw.l93_1x1;

/* Création de l'index spatial */

CREATE INDEX l93_1x1km_gidx
  ON repos.l93_1x1km USING GIST (geom);

/* Mise à jour de la table des couches cartographiques */

SELECT populate_geometry_columns();

