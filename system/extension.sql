-- L'adminpack fournit un certain nombre de fonctions de support que pgAdmin ou d'autres outils de gestion et d'administration peuvent utiliser pour fournir des fonctionnalités supplémentaires, comme la gestion à distance de journaux applicatifs. 
create
	EXTENSION IF NOT EXISTS adminpack;

-- installation de postgis avec son extension sfcgal pour des calculs topographiques avancés
-- Prerequis, il conviedra d'avoir préalablement installé correctement le serveur linux, voici un lien pour la configuration du systeme sous CentOS7
-- http://www.postgresonline.com/journal/archives/362-An-almost-idiots-guide-to-install-PostgreSQL-9.5,-PostGIS-2.2-and-pgRouting-2.1.0-with-Yum.html
create
	EXTENSION IF NOT EXISTS postgis;

create
	EXTENSION IF NOT EXISTS postgis_topology;

create
	EXTENSION IF NOT EXISTS postgis_sfcgal;

-- installation des d'accès à des données distantes, les "Foreign Data Wrappers"
-- Depuis un fichier texte : file_fdw;
create
	EXTENSION IF NOT EXISTS file_fdw;

-- Depuis une autre base PostgreSQL;
create
	EXTENSION IF NOT EXISTS postgres_fdw;

-- Depuis des données en ligne comme les serveurs WFS
create
	EXTENSION IF NOT EXISTS ogr_fdw;

-- Fin de l'installation des extensions FDW
-- Pour la réalisation de tableaux croisés
create
	EXTENSION IF NOT EXISTS tablefunc;
