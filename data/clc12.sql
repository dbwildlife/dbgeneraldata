create
	table
		dict.clc12_lev1(
			code12_lev1 integer primary key not null,
			libfr_lev1 varchar,
			liben_lev1 varchar,
			r_lev1 integer,
			v_lev1 integer,
			b_lev1 integer
		);
		
comment on table dict.clc12_lev1 is 'Corine Land Cover 2012 - Table des correspondances pour le niveau 1';

insert into dict.clc12_lev1 values
	(1,'Territoires artificialisés','Artificial surfaces',230,000,077),
	(2,'Territoires agricoles','Agricultural areas',255,255,168),
	(3,'Forêts et milieux semi-naturels','Forest and semi natural areas',128,255,000),
	(4,'Zones humides','Wetlands',166,166,255),
	(5,'Surfaces en eau','Water bodies',000,204,242);

create
	table
		dict.clc12_lev2(
			code12_lev2 integer primary key not null,
			libfr_lev2 varchar,
			liben_lev2 varchar,
			r_lev2 integer,
			v_lev2 integer,
			b_lev2 integer
		);
		
comment on table dict.clc12_lev2 is 'Corine Land Cover 2012 - Table des correspondances pour le niveau 2';		

insert
	into
		dict.clc12_lev2
	values
	(11,'Zones urbanisées','Urban fabric',230,000,077),
	(12,'Zones industrielles ou commerciales et réseaux de communication','Industrial, commercial and transport units',204,077,242),
	(13,'Mines, décharges et chantiers','Mine, dump and construction sites',166,000,204),
	(14,'Espaces verts artificialisés, non agricoles','Artificial, non-agricultural vegetated areas',255,166,255),
	(21,'Terres arables','Arable land',255,255,168),
	(22,'Cultures permanentes','Permanent crops',230,128,000),
	(23,'Prairies','Pastures',230,230,077),
	(24,'Zones agricoles hétérogènes','Heterogeneous agricultural areas',255,230,166),
	(31,'Forêts','Forests',128,255,000),
	(32,'Milieux à végétation arbustive et/ou herbacée','Scrub and/or herbaceous vegetation associations',204,242,077),
	(33,'Espaces ouverts, sans ou avec peu de végétation','Open spaces with little or no vegetation',230,230,230),
	(41,'Zones humides intérieures','Inland wetlands',166,166,255),
	(42,'Zones humides côtières','Coastal wetlands',204,204,255),
	(51,'Eaux continentales','Inland waters',000,204,242),
	(52,'Eaux maritimes','Marine waters',000,255,166);



		

create
	table
		dict.clc12_lev3(
			code12_lev3 integer primary key not null,
			libfr_lev3 varchar,
			liben_lev3 varchar,
			r_lev3 integer,
			v_lev3 integer,
			b_lev3 integer
		);

comment on table dict.clc12_lev3 is 'Corine Land Cover 2012 - Table des correspondances pour le niveau 3';		
		
insert
	into
		dict.clc12_lev3
	values
	(111,'Tissu urbain continu','Continuous urban fabric',230,000,077),
	(112,'Tissu urbain discontinu','Discontinuous urban fabric',255,000,000),
	(121,'Zones industrielles ou commerciales et installations publiques','Industrial or commercial units and public facilities',204,077,242),
	(122,'Réseaux routier et ferroviaire et espaces associés','Road and rail networks and associated land',204,000,000),
	(123,'Zones portuaires','Port areas',230,204,204),
	(124,'Aéroports','Airports',230,204,230),
	(131,'Extraction de matériaux','Mineral extraction sites',166,000,204),
	(132,'Décharges','Dump sites',166,077,000),
	(133,'Chantiers','Construction sites',255,077,255),
	(141,'Espaces verts urbains','Green urban areas',255,166,255),
	(142,'Equipements sportifs et de loisirs','Sport and leisure facilities',255,230,255),
	(211,'Terres arables hors périmètres d''irrigation','Non-irrigated arable land',255,255,168),
	(212,'Périmètres irrigués en permanence','Permanently irrigated land',255,255,000),
	(213,'Rizières','Rice fields',230,230,000),
	(221,'Vignobles','Vineyards',230,128,000),
	(222,'Vergers et petits fruits','Fruit trees and berry plantations',242,166,077),
	(223,'Oliveraies','Olive groves',230,166,000),
	(231,'Prairies et autres surfaces toujours en herbe à usage agricole','Pastures, meadows and other permanent grasslands under agricultural use',230,230,077),
	(241,'Cultures annuelles associées à des cultures permanentes','Annual crops associated with permanent crops',255,230,166),
	(242,'Systèmes culturaux et parcellaires complexes','Complex cultivation patterns',255,230,077),
	(243,'Surfaces essentiellement agricoles, interrompues par des espaces naturels importants','Land principally occupied by agriculture, with significant areas of natural vegetation',230,204,077),
	(244,'Territoires agroforestiers','Agro-forestry areas',242,204,166),
	(311,'Forêts de feuillus','Broad-leaved forest',128,255,000),
	(312,'Forêts de conifères','Coniferous forest',000,166,000),
	(313,'Forêts mélangées','Mixed forest',077,255,000),
	(321,'Pelouses et pâturages naturels','Natural grasslands',204,242,077),
	(322,'Landes et broussailles','Moors and heathland',166,255,128),
	(323,'Végétation sclérophylle','Sclerophyllous vegetation',166,230,077),
	(324,'Forêt et végétation arbustive en mutation','Transitional woodland-shrub',166,242,000),
	(331,'Plages, dunes et sable','Beaches, dunes, sands',230,230,230),
	(332,'Roches nues','Bare rocks',204,204,204),
	(333,'Végétation clairsemée','Sparsely vegetated areas',204,255,204),
	(334,'Zones incendiées','Burnt areas',000,000,000),
	(335,'Glaciers et neiges éternelles','Glaciers and perpetual snow',166,230,204),
	(411,'Marais intérieurs','Inland marshes',166,166,255),
	(412,'Tourbières','Peat bogs',077,077,255),
	(421,'Marais maritimes','Coastal salt marshes',204,204,255),
	(422,'Marais salants','Salines',230,230,255),
	(423,'Zones intertidales','Intertidal flats',166,166,230),
	(511,'Cours et voies d''eau','Water courses',000,204,242),
	(512,'Plans d''eau','Water bodies',128,242,230),
	(521,'Lagunes littorales','Coastal lagoons',000,255,166),
	(522,'Estuaires','Estuaries',166,255,230),
	(523,'Mers et océans','Sea and ocean',230,242,255);

create
	table
		dict.clc12_lev4_dom(
			code12_lev4 integer primary key not null,
			libfr_lev4 varchar,
			liben_lev4 varchar,
			r_lev4 integer,
			v_lev4 integer,
			b_lev4 integer
		);

comment on table dict.clc12_lev4_dom is 'Corine Land Cover 2012 - Table des correspondances pour le niveau 4 pour les DOM uniquement';
		
insert
	into
		dict.clc12_lev4_dom
	values
	(1110,'Tissu urbain continu','Continuous urban fabric',230,000,077),
	(1120,'Tissu urbain discontinu','Discontinuous urban fabric',255,000,000),
	(1210,'Zones industrielles ou commerciales et installations publiques','Industrial or commercial units and public facilities',204,077,242),
	(1220,'Réseaux routier et ferroviaire et espaces associés','Road and rail networks and associated land',204,000,000),
	(1230,'Zones portuaires','Port areas',230,204,204),
	(1240,'Aéroports','Airports',230,204,230),
	(1310,'Extraction de matériaux','Mineral extraction sites',166,000,204),
	(1320,'Décharges','Dump sites',166,077,000),
	(1330,'Chantiers','Construction sites',255,077,255),
	(1410,'Espaces verts urbains','Green urban areas',255,166,255),
	(1420,'Equipements sportifs et de loisirs','Sport and leisure facilities',255,230,255),
	(2111,'Terres arables hors périmètres d''irrigation','Non-irrigated arable land',255,255,168),
	(2112,'Canne à sucre','Sugar cane',255,211,127),
	(2120,'Périmètres irrigués en permanence','Permanently irrigated land',255,255,000),
	(2130,'Rizières','Rice fields',230,230,000),
	(2210,'Vignobles','Vineyards',230,128,000),
	(2221,'Vergers et petits fruits','Fruit trees and berry plantations',242,166,077),
	(2222,'Bananeraies','Banana plantations',255,255,190),
	(2223,'Palmeraies','Palm groves',255,255,115),
	(2224,'Caféiers','Coffee trees',255,170,000),
	(2230,'Oliveraies','Olive groves',230,166,000),
	(2310,'Prairies et autres surfaces toujours en herbe à usage agricole','Pastures, meadows and other permanent grasslands under agricultural use',230,230,077),
	(2410,'Cultures annuelles associées à des cultures permanentes','Annual crops associated with permanent crops',255,230,166),
	(2420,'Systèmes culturaux et parcellaires complexes','Complex cultivation patterns',255,230,077),
	(2430,'Surfaces essentiellement agricoles, interrompues par des espaces naturels importants','Land principally occupied by agriculture, with significant areas of natural vegetation',230,204,077),
	(2440,'Territoires agroforestiers','Agro-forestry areas',242,204,166),
	(3111,'Forêts de feuillus','Broad-leaved forest',128,255,000),
	(3112,'Mangroves','Mangrove forest',000,168,132),
	(3120,'Forêts de conifères','Coniferous forest',000,166,000),
	(3130,'Forêts mélangées','Mixed forest',077,255,000),
	(3210,'Pelouses et pâturages naturels','Natural grasslands',204,242,077),
	(3220,'Landes et broussailles','Moors and heathland',166,255,128),
	(3230,'Végétation sclérophylle','Sclerophyllous vegetation',166,230,077),
	(3240,'Forêt et végétation arbustive en mutation','Transitional woodland-shrub',166,242,000),
	(3310,'Plages, dunes et sable','Beaches, dunes, sands',230,230,230),
	(3320,'Roches nues','Bare rocks',204,204,204),
	(3330,'Végétation clairsemée','Sparsely vegetated areas',204,255,204),
	(3340,'Zones incendiées','Burnt areas',000,000,000),
	(3350,'Glaciers et neiges éternelles','Glaciers and perpetual snow',166,230,204),
	(4110,'Marais intérieurs','Inland marshes',166,166,255),
	(4120,'Tourbières','Peat bogs',077,077,255),
	(4210,'Marais maritimes','Coastal salt marshes',204,204,255),
	(4220,'Marais salants','Salines',230,230,255),
	(4230,'Zones intertidales','Intertidal flats',166,166,230),
	(5111,'Cours et voies d''eau','Water courses',000,204,242),
	(5112,'Cours et voies d''eau temporaires','Temporary water courses',190,232,255),
	(5120,'Plans d''eau','Water bodies',128,242,230),
	(5210,'Lagunes littorales','Coastal lagoons',000,255,166),
	(5220,'Estuaires','Estuaries',166,255,230),
	(5230,'Mers et océans','Sea and ocean',230,242,255);


	

