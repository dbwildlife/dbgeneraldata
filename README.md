# vnworkdb
Ce projet concerne le développement d'un outil d'exploitation des données VisioNature :

**ATTENTION, PROJET ACTUELLEMENT NON FONCTIONNEL EN COURS DE DEVELOPPEMENT**

## Prérequis

A l'heure actuelle, ce projet est basé sur un serveur Linux sous CentOS 7 64 (cf. le [wiki](https://framagit.org/dbwildlife/dbgeneraldata/wikis/home) pour la configuration du serveur).
Préalablement à l'éxécution de ces outils d'initialisation de la base de donnée, il convient de procéder à l'installation de PostgreSQL/Postgis et de plusieurs autres extensions sur PostGIS.

A cet effet, vous pouvez suivre le site internet suivant sur [postgresonline.com](http://www.postgresonline.com/journal/archives/362-An-almost-idiots-guide-to-install-PostgreSQL-9.5,-PostGIS-2.2-and-pgRouting-2.1.0-with-Yum.html)

```
Racine du projet
.
├── foreign_server > ajout des serveurs de données distants
│   ├── dreal_ra_server.sql
│   ├── inpn_server.sql
│   ├── sandre_server.sql
│   ├── soes_server.sql
│   └── taxref_texfile_server.sql
├── foreign_table > Création des tables distantes et importation en local
│   ├── brgm_risque.sql
│   ├── dreal_ra_znir.sql
│   ├── inpn_taxref.sql
│   ├── inpn_znir.sql
│   ├── README.MD
│   ├── sandre_secours.sql
│   └── sandre.sql
├── initdb.sh > script d''initialisation du serveur.
├── LICENSE
├── README.md
├── system > requêtes d''initialisation du serveur
│   └── extension.sql > installation des extensions requises
└── taxref
    └── taxref_prot.sql
```
## Installation de PostgreSQL 9.6 et des extensions

Sur un serveur CentOS 7

```sh
# Installation de postgresql 9.6 et des librairies nécessaires pour les extensions
rpm -Uvh https://yum.postgresql.org/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm
yum install postgresql96-server postgresql96 postgresql96-devel postgis2_96 gdal-devel
#Indiquer au système l'emplacement des outils postgresql (psql, createdb, etc.)
PATH=$PATH:/usr/pgsql-9.6/bin/
# Compilation et installation de l'extension ogr_fdw
cd /tmp
git clone https://github.com/pramsey/pgsql-ogr-fdw
cd pgsql-ogr-fdw/
make
make install
```
## [Wiki du projet](https://framagit.org/dbwildlife/dbgeneraldata/wikis/home)
