create
	materialized view status.matrice_protection as /* pour version >= 9.3 */
	--CREATE TABLE status.matrice_protection AS		/* pour version  < 9.3 */
	select
		distinct cd_nom as cd_ref,
		string_agg(
			case
				when protection_especes.cd_protection::text = 'CDH%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as dir_hab,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'CDO%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as dir_ois,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'NV%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_veg,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'nar%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_ra,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'ni%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_ins,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'nmo%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_mol,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'nm%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_mamm,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'no%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_ois,
		string_agg(
			case
				when protection_especes.cd_protection::text ~~* 'np%'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pn_poi,
		string_agg(
			case
				when protection_especes.cd_protection::text = 'RV91'::text then protection_especes_types.article
				else null::character varying
			end::text,
			', '::text
		) as pr_veg
	from
		referentiel.taxref_protsp join status.protection_especes_types
			using(cd_protection)
	where
		protection_especes.cd_protection::text ~~* 'cdh%'::text
		or protection_especes.cd_protection::text ~~* 'cdo%'::text
		or protection_especes.cd_protection::text ~~* 'nv%'::text
		or protection_especes.cd_protection::text ~~* 'nar%'::text
		or protection_especes.cd_protection::text ~~* 'ni%'::text
		or protection_especes.cd_protection::text ~~* 'nmo%'::text
		or protection_especes.cd_protection::text ~~* 'nm%'::text
		or protection_especes.cd_protection::text ~~* 'no%'::text
		or protection_especes.cd_protection::text ~~* 'np%'::text
		or protection_especes.cd_protection::text = 'RV91'::text
	group by
		protection_especes.cd_nom with data /* pour version >= 9.3, à commenter sinon */;
		
		/* On commente les colonnes */
		comment on
		column status.matrice_protection.cd_ref is 'le cd_ref concerné';

comment on
column status.matrice_protection.dir_hab is 'article de la directive habitat';

comment on
column status.matrice_protection.dir_ois is 'article de la directive oiseaux';

comment on
column status.matrice_protection.cd_ref is 'le cd_ref concerné';

comment on
column status.matrice_protection.pn_ins is 'insectes protégés au niveau national';

comment on
column status.matrice_protection.pn_ra is 'reptiles et amphibiens protégés au niveau national';

comment on
column status.matrice_protection.pn_mol is 'mollusques protégés au niveau national';

comment on
column status.matrice_protection.pn_mamm is 'mammifères protégés au niveau national';

comment on
column status.matrice_protection.pn_ois is 'oiseaux protégés au niveau national';

comment on
column status.matrice_protection.pn_poi is 'poissons protégés au niveau national';

comment on
column status.matrice_protection.pr_veg is 'plantes protégées en région';

/*on indexe le champs cd_ref qui sera souvent utilisé pour la jointure
	On peut aussi crééer une contrainte de clé étrangère pointant vers la colonne cd_nom de la table status.taxref
	Cela créera implicitement l'index, en plus de renforcer l'intégrité de la donnée */
create
	unique index on
	status.matrice_protection(cd_ref);
