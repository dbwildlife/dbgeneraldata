﻿-- with evn.places as table with places data
-- and boundaries.departments (deptartment number 42) as area covered by visionature portail


CREATE MATERIALIZED VIEW evn.white_area AS 
WITH buff_places AS (
         SELECT	st_union(st_buffer(p.geom, 700::double precision)) AS geom
         FROM	temp.places_loire p
         ), 
diff_places AS (
        SELECT	(st_dump(st_difference(d.st_union, t.geom))).path[1] AS id,
		(st_dump(st_difference(d.st_union, t.geom))).geom AS geom
        FROM 	buff_places t,
		boundaries.departments_2016 d
        WHERE 	d.code_dept::text = '42'::text
	)
SELECT	diff_places.id,
	diff_places.geom
FROM	diff_places;
