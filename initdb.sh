#!/bin/bash
cmd=$1

# Load configuration file, if present, else ask for configuration
initdb_conf=~/.evn.conf
unset config # clear parameter array
typeset -A config # init array

# echo "commande = $cmd"
if [[ -f $initdb_conf ]]  # Check if exists and load existing config
then
    echo "Chargement de la configuration"
    # Parse configuration file
    while read line
    do
        # echo $line
        if echo $line | grep -F = &>/dev/null
        then
            varname=$(echo "$line" | cut -d '=' -f 1)
            config[$varname]=$(echo "$line" | cut -d '=' -f 2-)
        fi
    done < $initdb_conf
else
    echo "Configuration initiale"
    cmd=config
    # Prepare default values
    config[evn_db_host]="localhost"
    config[evn_db_port]="5432"
    config[evn_logging]="INFO"
fi

read -p "Attention, ceci va modifier votre fichier d'initialisation de votre base de donnée, voulez-vous continuer (y/n)?" choice
case "$choice" in 
    y|Y ) echo "yes";;
    n|N ) echo "no";;
    * ) echo "invalid";;
esac

cp InitDB.sql InitDB.tmp
    sed -i -e "s/evn_db_name/${config[evn_db_name]}/" InitDB_extensions.tmp
       sed -i -e "s/evn_db_schema/${config[evn_db_schema]}/" InitDB_extensions.tmp
        sed -i -e "s/evn_db_group/${config[evn_db_group]}/" InitDB_extensions.tmp
        sed -i -e "s/evn_db_user/${config[evn_db_user]}/" InitDB_extensions.tmp
        sed -i -e "s/evn_db_pw/${config[evn_db_pw]}/" InitDB_extensions.tmp
        mv InitDB.tmp Init_extensions_${config[evn_db_name]}.sql

        echo "Pour (re)créer la base de données, exécutez la commande suivante depuis le compte postgres"
        echo "$ psql -f $(pwd)/Init_extensions_${config[evn_db_name]}.sql"

    exit 0
