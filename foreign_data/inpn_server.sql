﻿/************************************
*	Création d'un serveur étranger		*
*	depuis les données WFS de l'INPN  *
*************************************/

/* Création de la connection aux serveur de données WFS de l'INPN:
*       ZNIRS (Znieff, N2000, Zico, Rnn, Rnr, Pnr, etc.) */

CREATE SERVER inpn_infogeo_wfs
FOREIGN DATA WRAPPER ogr_fdw
OPTIONS (datasource 'WFS:http://ws.carmencarto.fr/WFS/119/fxx_inpn?', FORMAT 'WFS');
ALTER SERVER inpn_infogeo_wfs
OWNER TO evn_db_group;

/* Création de la connection aux serveur de données WFS de l'INPN:
*       Grilles (mailles 1km, 5km, 10km) */

CREATE SERVER inpn_grillegeo_wfs
FOREIGN DATA WRAPPER ogr_fdw
OPTIONS (datasource 'WFS:http://ws.carmencarto.fr/WFS/119/fxx_grille?', FORMAT 'WFS');

ALTER SERVER inpn_grillegeo_wfs
OWNER TO evn_db_group;

/* Création de la connection aux serveur de données WFS de l'INPN:
*       Référentiels (zones biogéo, départements, bathymétrie) */

CREATE SERVER inpn_refgeo_wfs
FOREIGN DATA
WRAPPER ogr_fdw
OPTIONS (
  datasource 'WFS:http://ws.carmencarto.fr/WFS/119/fxx_ref?',
FORMAT 'WFS' );

ALTER SERVER inpn_infogeo_wfs
OWNER TO evn_db_group;

/************************************
*   Création des tables distantes   *
*************************************/


/*  Pour lister les tables du serveur depuis un terminal,
    utilisez le programme ogr_fdw_info (https://github.com/pramsey/pgsql-ogr-fdw)


Un exemple:

    # ogr_fdw_info -s WFS:http://ws.carmencarto.fr/WFS/119/fxx_grille?
    Layers:
      L93_5x5
      LA_10x10
      L93_10X10
      L93_1X1

    # ogr_fdw_info -s WFS:http://ws.carmencarto.fr/WFS/119/fxx_grille? -l L93_10x10

    CREATE SERVER myserver
      FOREIGN DATA WRAPPER ogr_fdw
      OPTIONS (
        datasource 'WFS:http://ws.carmencarto.fr/WFS/119/fxx_grille?',
        format 'WFS' );

    CREATE FOREIGN TABLE l93_10x10 (
      fid bigint,
      geom Geometry(Geometry,4326),
      gml_id varchar
    ) SERVER myserver
    OPTIONS (layer 'L93_10X10');

*/

/* Création d'un schema dédié
aux données des Foreign Data Wrappers */
CREATE SCHEMA IF NOT EXISTS fdw
  AUTHORIZATION lpo07;

/*
Création de la table distante des parcs naturels marins
*/
DROP FOREIGN TABLE IF EXISTS fdw.inpn_parc_naturel_marin;
CREATE FOREIGN TABLE fdw.inpn_parc_naturel_marin (
  fid      INT,
  geom     GEOMETRY,
  gml_id   TEXT,
  id_mnhn  TEXT,
  nom_site TEXT,
  url      TEXT
)
SERVER inpn_infogeo_wfs
OPTIONS (layer 'Parc_naturel_marin'
);

/*
Création d'un schéma destiné à recevoir l'ensemble des couches de "zones naturelles d'intérêt reconnu"
*/
CREATE SCHEMA IF NOT EXISTS znir
  AUTHORIZATION lpo07;

/*
Création de la table locale des parcs naturels marins
*/
CREATE TABLE znir.inpn_parc_naturel_marin AS
  (SELECT *
   FROM fdw.inpn_parc_naturel_marin);

ALTER TABLE znir.inpn_parc_naturel_marin
  ADD PRIMARY KEY (fid);
CREATE INDEX gidx_inpn_parc_naturel_marin
  ON znir.inpn_parc_naturel_marin USING GIST (geom);

-- Création de la table distante des parcs nationaux
DROP FOREIGN TABLE IF EXISTS fdw.inpn_parc_national;
CREATE FOREIGN TABLE fdw.inpn_parc_national (
  fid      INT,
  geom     GEOMETRY,
  gml_id   TEXT,
  id_mnhn  TEXT,
  nom_site TEXT,
  url      TEXT
)
SERVER inpn_infogeo_wfs
OPTIONS (layer 'Parcs_nationaux'
);

-- Création de la table locale des parcs nationaux
CREATE TABLE znir.inpn_parc_national AS
  (SELECT
     fid,
     st_setsrid(geom, 2154) geom,
     gml_id,
     id_mnhn,
     nom_site,
     url
   FROM fdw.inpn_parc_national);

ALTER TABLE znir.inpn_parc_national
  ADD PRIMARY KEY (fid);
CREATE INDEX gidx_inpn_parc_national
  ON znir.inpn_parc_national USING GIST (geom);

-- Création de la table distante des sites natura 2000
DROP FOREIGN TABLE IF EXISTS fdw.inpn_n2000_sic;
CREATE FOREIGN TABLE fdw.inpn_n2000_sic (
  fid      INT,
  geom     GEOMETRY,
  gml_id   TEXT,
  sitecode TEXT,
  sitename TEXT,
  url      TEXT
)
SERVER inpn_infogeo_wfs
OPTIONS (layer 'Sites_d_importance_communautaire'
);

--Création de la table locale des sites Natura 2000 (SIC)
CREATE TABLE znirs.inpn_n2000_sic AS
  (SELECT
     fid,
     st_setsrid(geom, 2154) geom,
     gml_id,
     sitecode,
     sitename,
     url
   FROM fdw.inpn_n2000_sic);

ALTER TABLE znirs.inpn_n2000_sic
  ADD PRIMARY KEY (fid);
CREATE INDEX gidx_inpn_n2000_sic
  ON znirs.inpn_n2000_sic USING GIST (geom);

-- Actualisation des géométries de la base de données
SELECT Populate_Geometry_Columns();


/* Création de la table des zones biogéographiques */

/* Création de la table distante */
CREATE FOREIGN TABLE fdw.inpn_ref_regbio (
  fid        BIGINT,
  geom       GEOMETRY(geometry, 2154),
  gml_id     VARCHAR,
  nom_domain VARCHAR
) SERVER inpn_refgeo_wfs
OPTIONS (layer 'regbio'
);

/* Création de la table locale depuis la table distante */

CREATE TABLE IF NOT EXISTS referentiel.regbio_frmet AS
  SELECT *
  FROM fdw.inpn_ref_regbio;



