﻿ --/*********************************************/
--/	Création d'un serveur étranger		/
--/	depuis les données WFS de la DREAL RA	/
--/*********************************************/
-- Création du serveur des informations géographiques

create
	SERVER dreal_ra_biodiv_wfs foreign data WRAPPER ogr_fdw options(
		datasource 'WFS:http://ws.carmen.developpement-durable.gouv.fr/WFS/30/NATURE_PAYSAGE_BIODIVERSITE_RA?',
		format 'WFS'
	);

alter SERVER dreal_ra_biodiv_wfs OWNER to evn_db_group;
