﻿ --/*********************************************/
--/	Création d'un serveur étranger		/
--/	depuis les données WFS de la DREAL RA	/
--/*********************************************/
-- Création du serveur des informations géographiques

CREATE
	SERVER sandre_wfs FOREIGN DATA WRAPPER ogr_fdw OPTIONS(
		datasource 'WFS:http://services.sandre.eaufrance.fr/geo/eth_FXX?Request=GetCapabilities&SERVICE=WFS&VERSION=1.1.0',
		format 'WFS'
	);

ALTER SERVER sandre_wfs OWNER TO evn_db_group;
