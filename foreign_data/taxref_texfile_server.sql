--/**********************************/
--/ Création d'un serveur étranger   /
--/ depuis les données texte TAXREF  /
--/**********************************/
-- Source http://si.cenlr.org/postgresql_fdw_taxref
-- Important, ce serveur nécessite d'avoir préalablement téléchargé les données PostgreSQL. cf. fichier foreign_table/taxref_table.sql 
create
	EXTENSION file_fdw;

create
	SERVER txtfile_srv foreign data WRAPPER file_fdw;
