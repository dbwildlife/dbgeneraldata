﻿ --/*********************************/
--/	Création d'un serveur étranger	/
--/	depuis les données WFS du SOeS  /
--/*********************************/
-- Création du serveur des couches CorineLandCover

CREATE
	SERVER soes_clc_wfs FOREIGN DATA WRAPPER ogr_fdw OPTIONS(
		datasource 'WFS:http://clc.developpement-durable.gouv.fr/geoserver/wfs?',
		format 'WFS'
	);

ALTER SERVER soes_clc_wfs OWNER TO evn_db_group;


